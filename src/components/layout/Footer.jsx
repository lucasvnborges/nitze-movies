import styled from "styled-components";
import { neutral, typeScale } from "../token";

const Footer = () => {
  return (
    <Container>
      <p>
        🧑‍💻 Developed by: <a href="https://gitlab.com/lucasvnborges" className="link" target="_blank" rel="noreferrer">Lucas Borges</a>
      </p>
      <p>
        Lucasvnborges@gmail.com
      </p>
      <p>
        🤖 API service: <a href="https://www.themoviedb.org/" className="link" target="_blank" rel="noreferrer">themoviedb.org</a>
      </p>
    </Container>
  );
};

const Container = styled.footer`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: ${typeScale.helper};
  background-color: ${neutral[50]};

  padding: 4rem 0;

  .link {
    text-decoration: underline;
  }
`;
export default Footer;

import { useEffect } from "react";
import Presenter from "./Presenter";

const Container = () => {
  const getData = async () => {};

  useEffect(() => {
    getData();
  }, []);

  return <Presenter />;
};

export default Container;

import styled from "styled-components";

//import utils
import { getGenre } from "../../util/getGenres";

//import components
import Indicator from "../../components/placeholder/Indicator";
import { Grid2 } from "../../components/Grid";
import Poster from "../../components/Poster";
import Pagination2 from "../../components/Pagination2";
import { SearchIcon } from "../../assets/Icons";
import { primaryColors, neutral, fontSize, breakpoint } from "../../components/token";

const Search = (props) => {
  return (
    <Container>
      <Form onSubmit={props.onSubmit}>
        <Row>
          <SearchIcon width="20" height="20" color="#000" stroke="2" />
          <Input
            value={props.keyword}
            onChange={props.onChange}
            placeholder="Search Movies"
          />
        </Row>

        <SearchButton onClick={props.onSubmit}>
          Go search
        </SearchButton>
      </Form>

      {props.loading ? (
        <Indicator />
      ) : (
        <>
          {props.results && props.results.length > 0 && (
            <>
              <Grid2 title="Movie Results">
                {props.results.map((movie) => (
                  <Poster
                    key={movie.id}
                    id={movie.id}
                    imageUrl={movie.poster_path}
                    title={movie.title}
                    rating={movie.vote_average}
                    year={movie.release_date}
                    genre={getGenre(props.genres, movie.genre_ids)}
                    isMovie={true}
                  />
                ))}
              </Grid2>
              <Pagination2
                page={props.page}
                total_pages={props.total_pages}
                handlePage={(page) => props.handlePage(page)}
              />
            </>
          )}
        </>
      )}
    </Container>
  );
};

const Container = styled.div`
  width: 100%;
  max-width: 1140px;
  margin: 2rem auto 0;

  @media (max-width: 1200px) {
    padding: 0 2em;
  }

  @media (max-width: 425px) {
    padding: 0 1em;
  }
`;

const Form = styled.form`
  position: relative;
  margin-bottom: 50px;
  width: 100%;

  display: flex;
  flex-direction: row;
  align-items: center;
  
  @media ${breakpoint.m} {
    flex-direction: column;
  }

  svg {
    margin-left: 0.75rem;
  }
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex: 1;
`;

const Input = styled.input`
  all: unset;
  width: 100%;
  height: 100%;
  font-size: 28px;
  padding-left: 1rem;
`;

const SearchButton = styled.div`
  cursor: pointer;
  padding: 1rem 2rem;
  color: ${neutral[0]};
  border-radius: 0.75rem;
  font-size: ${fontSize.lg1};
  background-color: ${primaryColors.cornflower};

  @media ${breakpoint.m} {
    margin-top: 2rem;
    padding: 0.5rem 1rem;
  }
`;

export default Search;
